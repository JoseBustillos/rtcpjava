package com.example.rtsp;

public class RTPpacket {
    // tamaño del encabezado RTP:
    static int HEADER_SIZE = 12;
    // Campos que componen el encabezado RTP
    public int Version;
    public int Padding;
    public int Extension;
    public int CC;
    public int Marker;
    public int PayloadType;
    public int SequenceNumber;
    public int TimeStamp;
    public int Ssrc;
    // Bitstream del encabezado RTP
    public byte[] header;
    // tamaño de la carga útil RTP
    public int payload_size;
    // Bitstream de la carga RTP
    public byte[] payload;
    /* --------------------------
    Constructor de un objeto RTPpacket a partir de campos de encabezado y 
    flujo de bits de carga útil
    --------------------------*/
    public RTPpacket(int PType, int Framenb, int Time, byte[] data,
            int data_length) {
        // rellenar por defecto los campos de encabezado:
        Version = 2;
        Padding = 0;
        Extension = 0;
        CC = 0;
        Marker = 0;
        Ssrc = 0;
        // rellenar campos de encabezado cambiantes:
        SequenceNumber = Framenb;
        TimeStamp = Time;
        PayloadType = PType;
        // construir el encabezado bistream:
        // --------------------------
        header = new byte[HEADER_SIZE];
        header[1] = (byte) ((Marker << 7) | PayloadType);
        header[2] = (byte) (SequenceNumber >> 8);
        header[3] = (byte) (SequenceNumber);
        for (int i = 0; i < 4; i++) {
            header[7 - i] = (byte) (TimeStamp >> (8 * i));
        }
        for (int i = 0; i < 4; i++) {
            header[11 - i] = (byte) (Ssrc >> (8 * i));
        }
        payload_size = data_length;
        payload = new byte[data_length];
        payload = data;
    }

    /* --------------------------
    Constructor de un objeto RTPpacket del paquete 
    --------------------------*/
    public RTPpacket(byte[] packet, int packet_size) {
        // llenar los campos predeterminados:
        Version = 2;
        Padding = 0;
        Extension = 0;
        CC = 0;
        Marker = 0;
        Ssrc = 0;
        // compruebe si el tamaño total del paquete es menor que el tamaño del encabezado
        if (packet_size >= HEADER_SIZE) {
            // obtener el flujo de bits del encabezado:
            header = new byte[HEADER_SIZE];
            for (int i = 0; i < HEADER_SIZE; i++) {
                header[i] = packet[i];
            }
            // obtener el flujo de bits de la carga útil:
            payload_size = packet_size - HEADER_SIZE;
            payload = new byte[payload_size];
            for (int i = HEADER_SIZE; i < packet_size; i++) {
                payload[i - HEADER_SIZE] = packet[i];
            }
            // interpretar los campos cambiantes del encabezado:
            PayloadType = header[1] & 127;
            SequenceNumber = unsigned_int(header[3]) + 256
                    * unsigned_int(header[2]);
            TimeStamp = unsigned_int(header[7]) + 256 * unsigned_int(header[6])
                    + 65536 * unsigned_int(header[5]) + 16777216
                    * unsigned_int(header[4]);
        }
    }

    // --------------------------
    // getpayload: devuelve el bistream de la carga útil del RTPpacket y su tamaño
    // --------------------------
    public int getpayload(byte[] data) {
        for (int i = 0; i < payload_size; i++) {
            data[i] = payload[i];
        }
        return (payload_size);
    }

    // --------------------------
    // getpayload_length: devuelve la longitud de la carga útil
    // --------------------------
    public int getpayload_length() {
        return (payload_size);
    }

    // --------------------------
    // getlength: devuelve la longitud total del paquete RTP
    // --------------------------
    public int getlength() {
        return (payload_size + HEADER_SIZE);
    }

    // --------------------------
    // getpacket: devuelve el flujo de bits del paquete y su longitud
    // --------------------------
    public int getpacket(byte[] packet) {
        // construir el paquete = encabezado + carga útil
        for (int i = 0; i < HEADER_SIZE; i++) {
            packet[i] = header[i];
        }
        for (int i = 0; i < payload_size; i++) {
            packet[i + HEADER_SIZE] = payload[i];
        }
        // devolver el tamaño total del paquete
        return (payload_size + HEADER_SIZE);
    }
    // --------------------------
    // gettimestamp
    // --------------------------
    public int gettimestamp() {
        return (TimeStamp);
    }
    // --------------------------
    // getsequencenumber
    // --------------------------
    public int getsequencenumber() {
        return (SequenceNumber);
    }
    // --------------------------
    // getpayloadtype
    // --------------------------
    public int getpayloadtype() {
        return (PayloadType);
    }
    // --------------------------
    // imprimir encabezados sin el SSRC
    // --------------------------
    public void printheader() {
        for (int i = 0; i < (HEADER_SIZE - 4); i++) {
            for (int j = 7; j >= 0; j--) {
                if (((1 << j) & header[i]) != 0) {
                    System.out.print("1");
                } else {
                    System.out.print("0");
                }
            }
            System.out.print(" ");
        }
        System.out.println();
    }

    // devuelve el valor sin signo del entero de 8 bits nb
    static int unsigned_int(int nb) {
        if (nb >= 0) {
            return (nb);
        } else {
            return (256 + nb);
        }
    }

}
